<?php


namespace Ox3a\Fias\Command;


use Ox3a\Fias\Parser\AddrObjParser;
use Ox3a\Fias\Parser\HouseParser;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Zend\Db\Adapter\Adapter as DbAdapter;
use Zend\Db\TableGateway\TableGateway;

class UpdateCommand extends Command
{
    public static $defaultName = 'fias:update';

    /**
     * @var DbAdapter
     */
    private $db;
    private $skip = false;


    protected function configure()
    {
        $this
            ->setDescription('скачивание и обновление данные')
            ->setHelp('Если указана дата, то будут скачены изменения с этой даты до актуальной.
            Иначе будет скачен полынй дамп');

        $this->addArgument('date', InputArgument::OPTIONAL, 'дата с которой необходимо обновлять данные');
        $this->addArgument('skip', InputArgument::OPTIONAL, 'пропускать существующие');
    }


    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $date       = $input->getArgument('date') ?: '2019-10-07';
        $this->skip = (bool)$input->getArgument('skip');

        $folder = date('Ymd', strtotime($date));
        $url    = sprintf("http://data.nalog.ru/Public/Downloads/%s/fias_delta_xml.rar", $folder);
        $dir    = sprintf(__DIR__ . '/../../../tmp/%s/fias_delta_xml', $folder);

        $this->updateAddrObj($dir);
        // $this->updateHouse($dir);
    }


    private function updateAddrObj($dir)
    {
        $list = glob($dir . '/AS_ADDROBJ*.XML');

        $table = new TableGateway('ADDROBJ', $this->getDb());
        $i     = 0;

        $this->getDb()->getDriver()->getConnection()->beginTransaction();

        $parser = new AddrObjParser();
        $parser->setAction(function($data) use ($table, &$i) {
            if ($table->select(['AOID=?' => $data['AOID']])->current()) {
                if ($this->skip) {
                    return;
                }
                $table->update($data, ['AOID=?' => $data['AOID']]);
            } else {
                $table->insert($data);
            }
            $i++;

            if ($i == 10000) {
                $this->getDb()->getDriver()->getConnection()->commit();
                echo $i . PHP_EOL;
                $this->getDb()->getDriver()->getConnection()->beginTransaction();
            }
        });
        $this->getDb()->getDriver()->getConnection()->commit();
        $parser->parse($list[0]);
    }


    private function updateHouse(string $dir)
    {
        $list = glob($dir . '/AS_HOUSE*.XML');

        $parser = new HouseParser();
        $parser->setAction(function($data) {
            print_r($data);
        });
        $parser->parse($list[0]);
    }


    private function getDb()
    {
        if (!$this->db) {
            $this->db = new DbAdapter(include __DIR__ . '/../../../tmp/db.cfg.php');
        }

        return $this->db;
    }
}
