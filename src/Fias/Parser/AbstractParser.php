<?php


namespace Ox3a\Fias\Parser;


use XMLReader;

abstract class AbstractParser
{
    protected $nodeName = '';

    /**
     * @var callable
     */
    protected $action;


    public function parse($filePath)
    {
        $reader = new XMLReader();
        if (!$reader->open($filePath)) {
            throw new \RuntimeException("Не смогли открыть {$filePath}");
        }

        while ($reader->read()) {
            if ($reader->name == $this->nodeName) {
                $object = [];
                while ($reader->moveToNextAttribute()) {
                    $object[$reader->name] = $reader->value;
                }

                if ($this->action) {
                    call_user_func($this->action, $object);
                }
            }
        }

        $reader->close();
    }


    /**
     * @param callable $action
     */
    public function setAction(callable $action)
    {
        $this->action = $action;
    }


    /**
     *
     */
    public function clearAction()
    {
        $this->action = null;
    }
}
