#!/usr/bin/env php
<?php

use Ox3a\Fias\Command\UpdateCommand;
use Symfony\Component\Console\Application;

require __DIR__ . '/../vendor/autoload.php';

$application = new Application();

$application->add(new UpdateCommand());

$application->run();
